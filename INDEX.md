# TDE (Thomson-Davis Editor)

TDE is a simple, public domain, multi-file/multi-window binary and text file editor written for IBM PCs and close compatibles running DOS, Win32 (console)

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## TDE.LSM

<table>
<tr><td>title</td><td>TDE (Thomson-Davis Editor)</td></tr>
<tr><td>version</td><td>5.1w</td></tr>
<tr><td>entered&nbsp;date</td><td>2007-05-01</td></tr>
<tr><td>description</td><td>A simple, binary and text file editor</td></tr>
<tr><td>keywords</td><td>edit, editor, text</td></tr>
<tr><td>author</td><td>Frank Davis Jason Hood &lt;jadoxa -at- yahoo.com.au&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Jason Hood &lt;jadoxa -at- yahoo.com.au&gt;</td></tr>
<tr><td>platforms</td><td>DOS, FreeDOS, Win32 (console) Linux</td></tr>
<tr><td>copying&nbsp;policy</td><td>Public Domain</td></tr>
<tr><td>summary</td><td>TDE is a simple, public domain, multi-file/multi-window binary and text file editor written for IBM PCs and close compatibles running DOS, Win32 (console)</td></tr>
</table>
